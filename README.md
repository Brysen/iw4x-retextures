# IW4x Retextures

This repository provides a big collection of retextures for Call of Duty: Modern Warfare 2 (MW2).
I use this with the IW4x modded client for MW2. Among other things, it Provides external folders for replacing the game's image files.
These files will (probably) still work the vanilla build of MW2, but in 2022, I don't know why you would still use that.

## Installation

If you use IW4x, you will place the files in `userraw\images`. 
This folder is located in the game install folder.
If you don't have the folders, just make them.

## Files included

- .IWI files
- .PNG/TIF files

IWI files are the image files read by the game.
These are the ones that will be placed in the `userraw\images`.
The PNG/TIF files are the source files and are NOT used by the game.
These are included so that you can make your own changes if you choose or to get a general preview of the retexture until proper preview screenshots can be taken.
If you want to provide preview screenshots, that would be helpful.

## Camo Replacements

In the `camo` folder of this repository, there is replacement files for the weapon camos of the game.
They are separated by folder and both the image file that is applied to the weapon and a matching file for the create-a-class menu is included.
The menu file won't always match the weapon file, but it will be close enough to get an idea of the camo.
They won't be named properly to be direct replacements.

To have the camos be read by the game they need to match the names of the original files. 
The file names and what they corrispond to are provided below.
Rename the .iwi files, NOT the .png files.

`weapon_camo_arctic.iwi`, `weapon_camo_menu_arctic.iwi` = Arctic Camo


`weapon_camo_desert.iwi`, `weapon_camo_menu_desert.iwi` = Desert Camo


`weapon_camo_digital.iwi`, `weapon_camo_menu_digital.iwi` = Digital Camo


`weapon_camo_woodland.iwi`, `weapon_camo_menu_woodland.iwi` = Woodland Camo


`weapon_camo_red_urban.iwi`, `weapon_camo_menu_urban.iwi` = Urban Camo


`weapon_camo_red_tiger.iwi`, `weapon_camo_menu_red_tiger.iwi` = Red Tiger Camo


`weapon_camo_blue_tiger.iwi`, `weapon_camo_menu_blue_tiger.iwi` = Blue Tiger Camo


`weapon_camo_orange_fall.iwi`, `weapon_camo_menu_orange_fall.iwi` = Fall Camo


## Weapon Texture Replacements
In the `weapons` folder of this repository, there is replacement files for the base weapon textures for some of the weapons. 
I've mainly made replacements for secondary weapons because they can't have camos applied in MW2.
These should have the original weapon texture names already, so they should just drop right into `userraw\images` and work with no issues.

## Usage

I put these retextures in this repository so that you can play with them in MW2.
However, if you want to use these in your Call of Duty (or any other game) mod, you will need to contact me for permission.
These will NOT be used in any original games under any circumstances.

## Credits

Sam Bowman - Blender material nodes and tutorials

Dr. Blender - Blender material nodes and tutorials

Default Cube - Blender material nodes and tutorials

Marvel Comics - For the screenshot used to make the "Thanos" camo

Infinity Ward - Camo patterns from Call of Duty: Modern Warfare (2019) used in a few of the retextures

Gucci - Designs used to make the Gucci camo and secondary retextures