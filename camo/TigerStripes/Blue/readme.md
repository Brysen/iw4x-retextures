# Tiger Stripes: Blue

## Camo
<img src="weapon_camo_TigerStripes_Blue.png" width="512" />

## Create-A-Class Preview
![Create-A-Class Preview](weapon_camo_menu_TigerStripes_Blue.png)
