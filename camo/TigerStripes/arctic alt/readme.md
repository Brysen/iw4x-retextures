# Tiger Stripes: Arctic Alt

## Camo
<img src="weapon_camo_TigerStripes_Arctic_Alt.png" width="512" />

## Create-A-Class Preview
![Create-A-Class Preview](weapon_camo_menu_TigerStripes_Arctic_Alt.png)