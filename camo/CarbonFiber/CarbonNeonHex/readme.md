# Carbon Fiber: Neon Hex

## Camo
<img src="weapon_camo_Carbon_NeonHex.png" width="512" />

## Create-A-Class Preview
![Create-A-Class Preview](weapon_camo_menu_Carbon_NeonHex.png)
