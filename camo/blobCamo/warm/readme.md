# Blob Camo: warm

## Camo
<img src="weapon_camo_blob_warm.png" width="512" />

## Create-A-Class Preview
![Create-A-Class Preview](weapon_camo_menu_blob_warm.png)
